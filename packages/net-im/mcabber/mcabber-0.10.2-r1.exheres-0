# Copyright 2008 Ingmar Vanhassel <ingmar@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="A small Jabber console client."
DESCRIPTION="
mcabber includes features such as SSL support, MUC (Multi-User Chat) support,
history logging, command completion, OpenPGP encryption, OTR (Off-the-Record
Messaging) support and external action triggers."
HOMEPAGE="http://www.${PN}.com"
DOWNLOADS="${HOMEPAGE}/files/${PNV}.tar.bz2"

REMOTE_IDS="freecode:${PN}"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    crypt [[ description = [ PGP encryption according to XEP-0027 ] ]]
    idn
    otr [[ description = [ Off-the-Record end-to-end encryption ] ]]
    spell
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-libs/glib:2
        net-im/loudmouth[>=1.4.2]
        sys-libs/ncurses
        crypt? ( app-crypt/gpgme )
        idn? ( net-dns/libidn )
        otr? (
            dev-libs/libgcrypt[>=1.2.2]
            net-libs/libotr[>=3.1.0&<4]
        )
        spell? ( app-spell/aspell )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-enchant
    --disable-xep0022
    --enable-modules
    --enable-sigwinch
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'crypt gpgme'
    otr
    'spell aspell'
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    'idn libidn'
)
DEFAULT_SRC_INSTALL_EXTRA_DOCS=( mcabberrc.example )

